﻿
Imports AAE.Events
Imports AAE.Events.Services

Public Class news_index
    Inherits System.Web.UI.Page
    Dim mfi As New System.Globalization.DateTimeFormatInfo()

    Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not Page.IsPostBack() Then
            'dataFill()
        End If
    End Sub

    Private Sub dataFill()
        Dim m_event_svc As New DeptEventService()
        Dim events As IList(Of DeptEvent) = m_event_svc.GetByDate(DateTime.Now, DateTime.Now.AddYears(1), 2)


        Dim current_month As Int32 = -1
        Dim current_day As Int32 = -1
        Dim current_day_li As HtmlControl = Nothing
        Dim month_ul As HtmlGenericControl = Nothing
        Dim month_li As HtmlControl

        Dim div_uw_event_listing As HtmlGenericControl = Nothing
        Dim span As HtmlGenericControl = Nothing
        Dim a As HtmlAnchor = Nothing
        Dim lt As LiteralControl = Nothing
        For Each devent As DeptEvent In events
            If month_ul Is Nothing OrElse current_month <> devent.StartDate.Month Then
                month_li = FormatMonthGroup(mfi.GetMonthName(devent.StartDate.Month))
                current_month = devent.StartDate.Month
                month_ul = UwEventsUl()
                month_li.Controls.Add(month_ul)
                div_events.Controls.Add(month_li)
            End If

            If current_day_li Is Nothing OrElse current_day <> devent.StartDate.Day Then

                current_day_li = EventListingLi(devent.StartDate)
                month_ul.Controls.Add(current_day_li)
                current_day = devent.StartDate.Day
            End If

            div_uw_event_listing = New HtmlGenericControl("div")
            div_uw_event_listing.AddClass("uw-event-listing")

            span = New HtmlGenericControl("span")
            span.AddClass("uw-event-listing")
            a = New HtmlAnchor()
            a.InnerHtml = devent.Summary
            a.HRef = "//aae.wisc.edu/events/view/d" & devent.EventID
            span.Controls.Add(a)
            div_uw_event_listing.Controls.Add(span)

            span = New HtmlGenericControl("span")
            span.AddClass("uw-event-subtitle")
            span.InnerHtml = devent.SubCategory
            div_uw_event_listing.Controls.Add(span)

            span = New HtmlGenericControl("span")
            span.AddClass("uw-event-time")
            span.InnerHtml = devent.StartDate.ToString("h:mm tt").ToUpper()
            div_uw_event_listing.Controls.Add(span)

            lt = New LiteralControl()
            lt.Text = ", "
            div_uw_event_listing.Controls.Add(lt)

            span = New HtmlGenericControl("span")
            span.AddClass("uw-event-location")
            span.InnerHtml = devent.Location
            div_uw_event_listing.Controls.Add(span)

            current_day_li.Controls.Add(div_uw_event_listing)

            '            <div Class="uw-event-listing">
            '    <span Class="uw-event-title">
            '        <a href = "/events/view/S687" > An Experimental Test Of Gender Differences In Charitable Giving</a>
            '    </span><span class="uw-event-subtitle">Jordan van Rijn, Esteban Quiñones &amp; Brad Barham, University of Wisconsin - Madison</span>
            '    <span Class="uw-event-time">3:45 PM</span>, <span class="uw-event-location">Taylor-Hibbard Seminar Room (Rm103)</span>
            '</div>
        Next

    End Sub

    Private Function FormatMonthGroup(month As String) As HtmlControl
        Dim li As New HtmlGenericControl("li")
        Dim span As New HtmlGenericControl("span")
        span.AddClass("uw-event-month")
        span.InnerHtml = month
        li.Controls.Add(span)

        Return li
    End Function

    ''' <summary>
    ''' events container for a single day
    ''' </summary>
    ''' <returns></returns>
    Private Function UwEventsUl() As HtmlControl
        Dim ul As New HtmlGenericControl("ul")
        ul.AddClass("uw-events")
        Return ul
    End Function

    Private Function EventListingLi(theDate As DateTime) As HtmlControl
        Dim li As New HtmlGenericControl("li")
        li.AddClass("uw-event")

        Dim div As New HtmlGenericControl("div")
        div.AddClass("uw-event-date")

        Dim span As New HtmlGenericControl("span")
        span.AddClass("show-for-sr")
        span.InnerHtml = mfi.GetMonthName(theDate.Month)

        Dim lt As New LiteralControl()
        lt.Text = " " & theDate.Day

        div.Controls.Add(span)
        div.Controls.Add(lt)

        li.Controls.Add(div)

        Return li
    End Function
    '<li>
    '<span Class="uw-event-month">September</span>
    '    <ul Class="uw-events">
    '        <li Class="uw-event">
    '            <div Class="uw-event-date">
    '                <span Class="show-for-sr">September</span> 7
    '            </div>
    '            <div Class="uw-event-listing">
    '                <span Class="uw-event-title">
    '                    <a href = "/events/view/S687" > An Experimental Test Of Gender Differences In Charitable Giving</a>
    '                </span><span class="uw-event-subtitle">Jordan van Rijn, Esteban Quiñones &amp; Brad Barham, University of Wisconsin - Madison</span>
    '                <span Class="uw-event-time">3:45 PM</span>, <span class="uw-event-location">Taylor-Hibbard Seminar Room (Rm103)</span>
    '            </div>
    '            <div Class="uw-event-listing">
    '                <span Class="uw-event-title">
    '                    <a href = "/events/view/S687" > An Experimental Test Of Gender Differences In Charitable Giving</a>
    '                </span><span class="uw-event-subtitle">Jordan van Rijn, Esteban Quiñones &amp; Brad Barham, University of Wisconsin - Madison</span>
    '                <span Class="uw-event-time">3:45 PM</span>, <span class="uw-event-location">Taylor-Hibbard Seminar Room (Rm103)</span>
    '            </div>
    '            <div Class="uw-event-listing">
    '                <span Class="uw-event-title">
    '                    <a href = "/events/view/S687" > An Experimental Test Of Gender Differences In Charitable Giving</a>
    '                </span><span class="uw-event-subtitle">Jordan van Rijn, Esteban Quiñones &amp; Brad Barham, University of Wisconsin - Madison</span>
    '                <span Class="uw-event-time">3:45 PM</span>, <span class="uw-event-location">Taylor-Hibbard Seminar Room (Rm103)</span>
    '            </div>
    '        </li>
End Class
