﻿Imports System.Net.Mail

Partial Class contact_index
    Inherits System.Web.UI.Page

    Public Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.Click
        SendMail()
    End Sub


    Public Sub SendMail()

        Try
            Dim netMail As New MailMessage()
            netMail.From = New MailAddress("itsc@aae.wisc.edu", "ITSC Automated")

            netMail.Subject = "REDA Contact - " & txtFirstName.Value & " (" & txtEmail.Value & ")"

            netMail.Body = "First Name: " & txtFirstName.Value & Environment.NewLine & "Email: " & txtEmail.Value & Environment.NewLine & Environment.NewLine & txtText.Value


            netMail.To.Add(New MailAddress("bglinsmann@wisc.edu"))

            Dim smtpSend As New SmtpClient()
            smtpSend.Host = "mail.aae.wisc.edu"
            smtpSend.Port = 25
            smtpSend.Send(netMail)

        Catch ex As Exception

        Finally
            phForm.Visible = False
            phDone.Visible = True
        End Try




    End Sub
End Class
