﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PublicWebMVC.Models
{
    public class CallbackPageModel
    {
        public string Error { get; set; }
        public string ErrorDescription { get; set; }
    }
}
