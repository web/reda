﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PublicWebMVC.Models
{
    public class AuthorizePageModel
    {
        public bool IsAuthorized { get; set; }
    }
}
