﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using DCS.Salesforce;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using PublicWebMVC.Models;
using Salesforce.Common;
using Salesforce.Common.Models.Json;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PublicWebMVC.Controllers
{
    /// <summary>
    /// Controller for creating a properly formatted authorization URL to login to Salesforce
    /// </summary>
    public class authorizeController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly ISalesforceOAuthManager _oauthManager;

        public authorizeController(IConfiguration Configuration, ISalesforceOAuthManager oauthManager)
        {
            _configuration = Configuration;
            _oauthManager = oauthManager;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            AuthorizePageModel model = new AuthorizePageModel();
            model.IsAuthorized = !(_oauthManager.GetOAuthData().refresh_token == string.Empty);

            return View(model);
        }

        public IActionResult Login()
        {
            string url =
                Common.FormatAuthUrl(
                    _configuration.GetSection(SalesforceConfigurationSection.AuthorizationEndpointUrl).Value,
                    ResponseTypes.Code,
                    _configuration.GetSection(SalesforceConfigurationSection.ConsumerKey).Value,
                    HttpUtility.UrlEncode(_configuration.GetSection(SalesforceConfigurationSection.CallbackUrl).Value));

            return Redirect(url);
        }
    }
}
