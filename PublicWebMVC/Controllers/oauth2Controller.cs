﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PublicWebMVC.Controllers
{
    /// <summary>
    /// test controller for faking Salesforce authorization
    /// </summary>
    public class oauth2Controller : Controller
    {
        private readonly IConfiguration _configuration;

        public oauth2Controller(IConfiguration Configuration)
        {
            _configuration = Configuration;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult token(string access_token, string refresh_token, string grant_type, string code, string redirect_uri, string client_secret, string client_id)
        {

            string new_access_token = Guid.NewGuid().ToString();
            if (refresh_token == null)
            {
                refresh_token = Guid.NewGuid().ToString();
            }

            if (grant_type == "authorization_code" || grant_type == "refresh_token")
            {
                return Json(new {
                    access_token = new_access_token,
                    refresh_token = refresh_token,
                    id = "whoknows",
                    instance_url = "Instance URL"

                });
            } else
            {
                throw new Exception("Invalid grant type.");
            }

        }

        public IActionResult authorize(string redirect_uri)
        {
            return Redirect(redirect_uri + "?code=eric123");
        }
    }
}
