﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using DCS.Salesforce;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using PublicWebMVC.Models;
using Salesforce.Common;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PublicWebMVC.Controllers
{
    /// <summary>
    /// controller for handling Salesforce callback during the app authorization process
    /// </summary>
    public class contactController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly ISalesforceOAuthManager _oauthManager;

        public contactController(IConfiguration Configuration, ISalesforceOAuthManager oauthManager)
        {
            _configuration = Configuration;
            _oauthManager = oauthManager;
        }

        /// <summary>
        /// Callback referenced at Salesforce API
        /// </summary>
        /// <param name="code">Authorization code returned from Salesforce authentication service</param>
        [HttpGet()]
        public async Task<IActionResult> sfcallback(string display = "", string code = "", string error="", string error_description="")
        {

            if (String.IsNullOrEmpty(error))
            {
                try
                {
                    SalesforceTokenRequestData data = new SalesforceTokenRequestData(_configuration);

                    var auth = new AuthenticationClient();
                    await auth.WebServerAsync(data.consumer_key, data.consumer_secret, data.callback_url, code, data.token_request_url);

                    _oauthManager.SaveOAuthData(new SalesforceOAuthData(auth));


                    return Redirect("~/authorize/");
                }
                catch (Exception ex)
                {
                    CallbackPageModel pageModel = new CallbackPageModel()
                    {
                        Error = ex.Message,
                        ErrorDescription = ex.StackTrace
                    };
                    return View(pageModel);
                }
                
            } else
            {
                CallbackPageModel pageModel = new CallbackPageModel() {
                    Error = error,
                    ErrorDescription= error_description
                };
                return View(pageModel);
            }

            
        }

    }
}
