﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DCS.Salesforce;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using Salesforce.Force;

namespace PublicWebMVC.Pages
{
    public class testModel : PageModel
    {
        private readonly IConfiguration _configuration;
        private readonly SalesforceOAuthManager _settingsManager;

        public string Message { get; private set; }
        public testModel(IConfiguration configuration, SalesforceOAuthManager settingsManager)
        {
            _configuration = configuration;
            _settingsManager = settingsManager;
        }
        public async void OnGet()
        {


            SalesforceInteractionsCreator creator = new SalesforceInteractionsCreator(
                new SalesforceAPISettings(_configuration),
                _settingsManager.GetOAuthData(),
                new SalesforceTokenRequestData(_configuration)
                );

            creator.AccessTokenUpdated += AccessTokenUpdate;

            IRFIInteractionData data = new RFIInteractionData() {
                Academic_Interest__c = "a0Q1I000006hTT8UAM",
                Term__c = "a0C1I00000Bo6wL",
                Campaign__c = "7011I000000EmrNQAS",
                Campaign_Member_Status__c = "Sent",
                Opportunity_Stage__c = "Inquiry",
                Career__c = "Graduate",
                //Eloqua_Contact_Label__c = "DCS",
                Interaction_Source__c = "Web Form",
                Interaction_Subsource__c = "Masters/Capstone RFI",
                Email__c = "eric.dieckman@wisc.edu",
                First_Name__c = "Eric",
                Last_Name__c = "Dieckman",
                //Phone__c = "608-692-0945"
            };

            await creator.CreateInteractionAsync(data);

        }

        /// <summary>
        /// handles the access token updated event
        /// </summary>
        public void AccessTokenUpdate(object sender, AccessTokenUpdateEventArgs args) 
        {
            _settingsManager.SaveOAuthData(args.OAuthData);
        }
    }
}