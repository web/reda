﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using DCS.Salesforce;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

namespace PublicWebMVC.Pages
{
    public class contactModel : PageModel
    {
        [BindProperty]
        public ContactFormModel Contact { get; set; }

        private readonly IConfiguration _configuration;
        private readonly ISalesforceOAuthManager _oauthManager;
        public contactModel(IConfiguration configuration, ISalesforceOAuthManager oauthManager)
        {
            _configuration = configuration;
            _oauthManager = oauthManager;
        }

            public void OnGet()
        {

        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            //email backup
            try
            {
                using (var message = new MailMessage())
                {
                    //message.To.Add(new MailAddress("eric.dieckman@wisc.edu"));
                    message.To.Add(new MailAddress("bill.provencher@wisc.edu"));
                    message.ReplyToList.Add(new MailAddress(Contact.Email));
                    message.From = new MailAddress("itsc@aae.wisc.edu");
                    message.Subject = "REDA Contact - " + Contact.FirstName + " (" + Contact.Email + ")";
                    message.Body = @"THIS INFORMATION WAS ALSO SENT AUTOMATICALLY TO SALESFORCE" + Environment.NewLine + Environment.NewLine + 
                        "Name: " + Contact.FirstName + " " + Contact.LastName + Environment.NewLine + "Email: " + Contact.Email + Environment.NewLine + Environment.NewLine + Contact.Message;

                    using (var smtpClient = new SmtpClient("relay.mail.wisc.edu"))
                    {
                        smtpClient.Port = 25;
                        await smtpClient.SendMailAsync(message);
                    }
                }
            }
            catch
            {

            }
            finally
            {
                
            }


            //send to salesforce
            SalesforceInteractionsCreator creator = new SalesforceInteractionsCreator(
                                    new SalesforceAPISettings(_configuration),
                                    _oauthManager.GetOAuthData(),
                                    new SalesforceTokenRequestData(_configuration)
                                    );

            // subscribe to the access token updated event
            creator.AccessTokenUpdated += AccessTokenUpdate;

            //// dev data/values
            //IRFIInteractionData data = new RFIInteractionData()
            //{
            //    Academic_Interest__c = "a0Q4D000000kDYIUA2",
            //    Term__c = "a0C4D000006K2tqUAC",
            //    Campaign__c = "7014D0000002R5UQAU",
            //    Campaign_Member_Status__c = "Sent",
            //    Opportunity_Stage__c = "Inquiry",
            //    Career__c = "Graduate",
            //    Eloqua_Contact_Label_Contact__c = "DCS",
            //    Interaction_Source__c = "Web Form",
            //    Interaction_Subsource__c = "Masters/Capstone RFI",
            //    Email__c = Contact.Email,
            //    First_Name__c = Contact.FirstName,
            //    Last_Name__c = Contact.LastName,
            //    Phone_Contact__c = Contact.Phone,
            //    Candidate_Questions__c = Contact.Message
            //};

            //// UAT data/values
            //IRFIInteractionData data = new RFIInteractionData()
            //{
            //    Academic_Interest__c = "a0Q1I000006hTT8UAM",
            //    Term__c = "a0C1I00000Bo6wKUAR",
            //    Campaign__c = "701g00000010a5dAAA",
            //    Campaign_Member_Status__c = "Sent",
            //    Opportunity_Stage__c = "Inquiry",
            //    Career__c = "Graduate",
            //    Eloqua_Contact_Label_Contact__c = "DCS",
            //    Interaction_Source__c = "Web Form",
            //    Interaction_Subsource__c = "Masters/Capstone RFI",
            //    Email__c = Contact.Email,
            //    First_Name__c = Contact.FirstName,
            //    Last_Name__c = Contact.LastName,
            //    Phone_Contact__c = Contact.Phone,
            //    Candidate_Questions__c = Contact.Message
            //};

            // Production data/values
            IRFIInteractionData data = new RFIInteractionData()
            {
                Academic_Interest__c = "a0Q1I000006hTT8UAM",
                Term__c = "a0C1I00000Bo6waUAB",                 // Summer 2020 - updated 5/21/2019
                Campaign__c = "7011I000000EmrNQAS",
                Campaign_Member_Status__c = "Sent",
                Opportunity_Stage__c = "Inquiry",
                Career__c = "Graduate",
                Eloqua_Contact_Label_Contact__c = "DCS",
                Interaction_Source__c = "Web Form",
                Interaction_Subsource__c = "Masters/Capstone RFI",
                Email__c = Contact.Email,
                First_Name__c = Contact.FirstName,
                Last_Name__c = Contact.LastName,
                Phone_Contact__c = Contact.Phone,
                Candidate_Questions__c = Contact.Message,
                Remove_from_Eloqua_Welcome_Campaign__c = true

            };


            await creator.CreateInteractionAsync(data);

            return RedirectToPage("contact-successful");



        }

        /// <summary>
        /// handles the access token updated event
        /// </summary>
        public void AccessTokenUpdate(object sender, AccessTokenUpdateEventArgs args)
        {
            _oauthManager.SaveOAuthData(args.OAuthData);
        }
    }

    public class ContactFormModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Message { get; set; }
    }
}