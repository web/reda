﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace DCS.Salesforce
{
    /// <summary>
    /// Data required for a token request
    /// </summary>
    public class SalesforceTokenRequestData
    {

        public SalesforceTokenRequestData(string consumer_key, string consumer_secret, string callback_url, string token_request_url)
        {
            this.token_request_url = token_request_url;
            this.consumer_key = consumer_key;
            this.consumer_secret = consumer_secret;
            this.callback_url = callback_url;
        }
        public SalesforceTokenRequestData(IConfiguration configuration)
            :this(consumer_key: configuration.GetSection(SalesforceConfigurationSection.ConsumerKey).Value,
                 consumer_secret: configuration.GetSection(SalesforceConfigurationSection.ConsumerSecret).Value,
                 callback_url: configuration.GetSection(SalesforceConfigurationSection.CallbackUrl).Value,
                 token_request_url: configuration.GetSection(SalesforceConfigurationSection.TokenRequestEndpointUrl).Value
                 )
        {

        }

        public string token_request_url { get;  private set; }
        public string consumer_key { get;  private set; }
        public string consumer_secret { get;  private set; }
        public string callback_url { get;  private set; }

    }



}
