﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DCS.Salesforce
{
    /// <summary>
    /// Constants of configuration section names
    /// </summary>
    public static class SalesforceConfigurationSection
    {
        private static string Section = "Salesforce";
        public static string ConsumerKey = Section + ":ConsumerKey";
        public static string ConsumerSecret = Section + ":ConsumerSecret";
        public static string CallbackUrl = Section + ":CallbackUrl";
        public static string AuthorizationEndpointUrl = Section + ":AuthorizationEndpointUrl";
        public static string TokenRequestEndpointUrl = Section + ":TokenRequestEndpointUrl";
        public static string InstanceUrl = Section + ":InstanceUrl";
        public static string APIVersion = Section + ":APIVersion";
        public static string InteractionSObject = Section + ":InteractionSObject";
    }
}
