﻿using Salesforce.Common;
using Salesforce.Common.Models.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace DCS.Salesforce
{
    /// <summary>
    /// OAuth2 data for SalesForce API access
    /// </summary>
    public class SalesforceOAuthData
    {
        public SalesforceOAuthData()
        {

        }

        public SalesforceOAuthData(AuthToken authToken)
        {
            access_token = authToken.AccessToken;
            refresh_token = authToken.RefreshToken;
        }

        public SalesforceOAuthData(AuthenticationClient authClient)
        {
            access_token = authClient.AccessToken;
            refresh_token = authClient.RefreshToken;
        }

        public string access_token { get; set; } = "";
        public string refresh_token { get; set; } = "";
    }
}
