﻿using System.Collections.Generic;

namespace DCS.Salesforce
{
    public interface IRFIInteractionData
    {
        string Academic_Interest__c { get; set; }
        string Campaign__c { get; set; }
        string Campaign_Member_Status__c { get; set; }
        string Candidate_Questions__c { get; set; }
        string Career__c { get; set; }
        string Eloqua_Contact_Label_Contact__c { get; set; }
        string Email__c { get; set; }
        string First_Name__c { get; set; }
        string Interaction_Source__c { get; set; }
        string Interaction_Subsource__c { get; set; }
        string Last_Name__c { get; set; }
        string Opportunity_Stage__c { get; set; }
        string Phone_Contact__c { get; set; }
        string Term__c { get; set; }

    }
}