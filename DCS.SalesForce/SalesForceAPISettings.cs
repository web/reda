﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace DCS.Salesforce
{
    /// <summary>
    /// Object to store information for connecting to the Salesforce Lightning API
    /// </summary>
    public class SalesforceAPISettings
    {
        public SalesforceAPISettings(string consumer_key, string consumer_secret, string callback_url, 
                            string instance_url, string api_version, string interaction_sobject)
        {
            this.consumer_key = consumer_key;
            this.consumer_secret = consumer_secret;
            this.callback_url = callback_url;
            this.instance_url = instance_url;
            this.api_version = api_version;
            this.interaction_sobject = interaction_sobject;
        }

        public SalesforceAPISettings(IConfiguration configuration)
            :this (
                consumer_key: configuration.GetSection(SalesforceConfigurationSection.ConsumerKey).Value,
                consumer_secret: configuration.GetSection(SalesforceConfigurationSection.ConsumerSecret).Value,
                callback_url: configuration.GetSection(SalesforceConfigurationSection.CallbackUrl).Value,
                instance_url: configuration.GetSection(SalesforceConfigurationSection.InstanceUrl).Value,
                api_version: configuration.GetSection(SalesforceConfigurationSection.APIVersion).Value,
                interaction_sobject: configuration.GetSection(SalesforceConfigurationSection.InteractionSObject).Value
                 )
        {

        }

        public string consumer_key { get; private set; }
        public string consumer_secret { get; private set; }
        public string callback_url { get; private set; }
        public string instance_url { get; private set; }
        public string api_version { get; private set; }
        public string interaction_sobject { get; private set; }
    }
}
