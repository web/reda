﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
namespace DCS.Salesforce
{
    /// <summary>
    /// Class holding data for RFI Interaction SObject
    /// </summary>
    public class RFIInteractionData : IRFIInteractionData
    {
        public string Email__c { get; set; }
        public string First_Name__c { get; set; }
        public string Last_Name__c { get; set; }
        public string Academic_Interest__c { get; set; }
        public string Phone_Contact__c { get; set; }
        public string Term__c { get; set; }
        public string Career__c { get; set; }
        public string Opportunity_Stage__c { get; set; }
        public string Interaction_Source__c { get; set; }
        public string Interaction_Subsource__c { get; set; }
        public string Eloqua_Contact_Label_Contact__c { get; set; }
        public string Campaign__c { get; set; }
        public string Campaign_Member_Status__c {get; set; }
        public string Candidate_Questions__c { get; set; }
        public bool Remove_from_Eloqua_Welcome_Campaign__c { get; set; }

    }
}
