﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DCS.Salesforce
{
    /// <summary>
    /// EventArgs for Access Token Update events
    /// </summary>
    public class AccessTokenUpdateEventArgs : EventArgs
    {
        public SalesforceOAuthData OAuthData { get; set; }
    }
}
