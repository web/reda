﻿using System;
using System.Collections.Generic;
using System.Text;
using DCS.Salesforce.Data;
using Microsoft.Extensions.Configuration;
namespace DCS.Salesforce
{
    /// <summary>
    /// Manages storage and retrieval of persisted Salesforce settings
    /// </summary>
    public class SalesforceOAuthManager : ISalesforceOAuthManager
    {
        private readonly ISalesForceOAuthsDatastore _datastore;

        public SalesforceOAuthManager(IConfiguration configuration, ISalesForceOAuthsDatastore datastore)
        {

            APISettings = new SalesforceAPISettings(configuration);
            _datastore = datastore;

        }

        public SalesforceAPISettings APISettings { get; private set; }

        public SalesforceOAuthData GetOAuthData()
        {
            return _datastore.GetOAuthData();
        }

        public void SaveOAuthData(SalesforceOAuthData data)
        {
            _datastore.SaveOAuthData(data);
        }
    }
}
