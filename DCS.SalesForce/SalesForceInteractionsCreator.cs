﻿using Salesforce.Common;
using Salesforce.Common.Models.Json;
using Salesforce.Force;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DCS.Salesforce
{
    /// <summary>
    /// object responsible for creating an Interactions SObject at SalesForce
    /// </summary>
    /// <remarks>v40.0</remarks>
    public class SalesforceInteractionsCreator
    {
        private readonly string _version;
        private readonly string _sobject;
        private readonly SalesforceAPISettings _api_settings;
        private readonly SalesforceOAuthData _oauth_data;
        private readonly SalesforceTokenRequestData _token_request_data;
        private readonly string _base_url = "";

        public event EventHandler<AccessTokenUpdateEventArgs> AccessTokenUpdated;

        public SalesforceInteractionsCreator(SalesforceAPISettings api_settings, SalesforceOAuthData oauth_data, SalesforceTokenRequestData tokenRequestData)
        {
            _api_settings = api_settings;
            _oauth_data = oauth_data;
            _token_request_data = tokenRequestData;
            _base_url = string.Format("{0}/services/data/", _api_settings.instance_url);

            _sobject = api_settings.interaction_sobject;
            _version = api_settings.api_version;
        }

        /// <summary>
        /// Create an Interaction at Salesforce through Lightning API
        /// </summary>
        public async Task<SuccessResponse> CreateInteractionAsync(IRFIInteractionData record) {

            bool refreshToken = false;
            int try_refresh = 0;                    // tracks the number of attempts to obtain a new access token
            SuccessResponse response = null;

            // loop - tries to create an interation - if it fails with "Session expired or invalid",
            // it tries to refresh the access token and runs the creation again
            do
            {
                // run if access token needs to be refreshed
                if (refreshToken)
                {
                    var auth = new AuthenticationClient();
                    await auth.TokenRefreshAsync(_api_settings.consumer_key, _oauth_data.refresh_token,_api_settings.consumer_secret,_token_request_data.token_request_url);

                    _oauth_data.access_token = auth.AccessToken;

                    //trigger event to notify listeners that the OAuth data has been updated (needs to be persisted)
                    OnAccessTokenUpdated();
                    refreshToken = false;
                }

                try
                {
                    var client = new ForceClient(_base_url, _oauth_data.access_token, _version);
                    response = await client.CreateAsync(_sobject, record);

                }
                catch (ForceException ex)
                {
                    if (ex.Message == "Session expired or invalid")
                    {
                        // create fails - most likely do to an expired access token.  Try refreshing the access token
                        refreshToken = true;
                        try_refresh++;
                    }
                }

            } while (refreshToken && try_refresh < 2);

            return response;
        }

        /// <summary>
        /// Trigger method called to raise the <see cref="AccessTokenUpdated"/> event
        /// </summary>
        protected virtual void OnAccessTokenUpdated()
        {
            AccessTokenUpdated?.Invoke(this, new AccessTokenUpdateEventArgs() { OAuthData = _oauth_data });
        }
        
    }
}
