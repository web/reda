﻿namespace DCS.Salesforce
{
    public interface ISalesforceOAuthManager
    {
        SalesforceAPISettings APISettings { get; }

        SalesforceOAuthData GetOAuthData();
        void SaveOAuthData(SalesforceOAuthData data);
    }
}