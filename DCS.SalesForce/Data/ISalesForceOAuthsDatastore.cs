﻿namespace DCS.Salesforce.Data
{
    /// <summary>
    /// Interface for storing Salesforce OAuth information
    /// </summary>
    public interface ISalesForceOAuthsDatastore
    {
        SalesforceOAuthData GetOAuthData();
        void SaveOAuthData(SalesforceOAuthData data);
    }
}