﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SQLite;
using Dapper;
using System.Data;

namespace DCS.Salesforce.Data
{
    /// <summary>
    /// Data store for SalesForce OAuth information using SQLite
    /// </summary>
    public class SQLiteOAuthDatastore : ISalesForceOAuthsDatastore
    {
        private readonly string settings_filename = "settings.sqlite";

        public SQLiteOAuthDatastore()
        {

            if (!SettingsDBExists())
            {
                CreateSettingsFile();
                InitializeDatabase();
            }
        }

        /// <summary>
        /// Checks whether the SQLite database exists
        /// </summary>
        private bool SettingsDBExists()
        {
            return System.IO.File.Exists(settings_filename);
        }

        /// <summary>
        /// Creates the database file
        /// </summary>
        private void CreateSettingsFile()
        {
            SQLiteConnection.CreateFile(settings_filename);
        }

        /// <summary>
        /// initializes the settings database
        /// </summary>
        private void InitializeDatabase() {
            using (SQLiteConnection conn = new SQLiteConnection(string.Format("Data Source={0}",settings_filename)))
            {
                conn.Open();
                string sql = @"CREATE TABLE `settings` (`key`	TEXT NOT NULL,	`value`	TEXT, `updated`	TEXT DEFAULT (datetime('now','localtime')),	PRIMARY KEY(`key`));
                                INSERT INTO settings (key, value) VALUES ('access_token', '');
                                INSERT INTO settings(key, value) VALUES('refresh_token', ''); ";
                SQLiteCommand command = new SQLiteCommand(sql, conn);
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// persists the OAuth2 data
        /// </summary>
        public void SaveOAuthData(SalesforceOAuthData data)
        {
            using (SQLiteConnection conn = new SQLiteConnection(string.Format("Data Source={0}", settings_filename)))
            {
                conn.Open();
                string sql = @"UPDATE settings SET value=@access_token_value, updated=datetime('now','localtime') WHERE key='access_token';
                                UPDATE settings SET value=@refresh_token_value, updated=datetime('now','localtime') WHERE key='refresh_token';";
                conn.Execute(sql, new {
                    access_token_value = data.access_token,
                    refresh_token_value = data.refresh_token
                });
            }
        }

        /// <summary>
        /// Retrieves persisted OAuth2 data
        /// </summary>
        public SalesforceOAuthData GetOAuthData()
        {
            SalesforceOAuthData data = new SalesforceOAuthData();

            using (SQLiteConnection conn = new SQLiteConnection(string.Format("Data Source={0}", settings_filename)))
            {
                conn.Open();
                string sql = "SELECT * FROM settings;";
                using(IDataReader rdr = conn.ExecuteReader(sql))
                {
                    int ordinal_key = rdr.GetOrdinal("key");
                    int ordinal_value = rdr.GetOrdinal("value");
                    while (rdr.Read())
                    {
                        if (rdr.GetString(ordinal_key) == "access_token")
                        {
                            data.access_token = rdr.GetString(ordinal_value);
                        }
                        if (rdr.GetString(ordinal_key) == "refresh_token")
                        {
                            data.refresh_token = rdr.GetString(ordinal_value);
                        }
                    }
                }
            }

            return data;
        }
    }
}

